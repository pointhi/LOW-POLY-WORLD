//the OpenGL context
var gl = null;

//scene graph nodes
var root = null;
var terrain = null;

var showWireframe = false;

var cameraMovement = null;
var showAnimation = false;
var showDayNight = true;

const camera = new Camera();

var sun = null;

// handle continuous key presses
var keys = [];
window.onkeyup = function(e) {keys[e.code]=false;};
window.onkeydown = function(e) {keys[e.code]=true;};

//load the shader resources using a utility function
loadResources({
    vs_single: 'shader/single.vs.glsl',
    fs_single: 'shader/single.fs.glsl',

    grass: 'models/grass.obj',
    grass_mtl: 'models/grass.mtl',
    grass_billboard: 'assets/grass.png',

    plant: 'models/plant.obj',
    plant_mtl: 'models/plant.mtl',
    plant_billboard: 'assets/plant.png',


    tree3: 'models/tree3.obj',
    tree3_mtl: 'models/tree3.mtl',
    tree3_texture: 'assets/tree_texture.jpg',
    tree3_billbaord: 'assets/tree3.png',

    sun: 'assets/sun.png',

    hy: 'assets/hy.png',

    mushroom1: 'models/mushroom1.obj',
    mushroom1_mtl: 'models/mushroom1.mtl',
    mushroom1_billboard: 'assets/mushroom.png',


    lighthouse: 'models/lighthouse.obj',
    lighthouse_mtl: 'models/lighthouse.mtl',
    lighthouse_texture: 'assets/lighthouse.jpg',


    house2: 'models/house2.obj',
    house2_mtl: 'models/house2.mtl',
    house2_texture: 'assets/house2.png',

    arrow: 'assets/arrow.png',

    watertexture: 'assets/water.texture.png',
    worldtexture: 'assets/test.texture.jpg',
    heightmap: 'assets/test.heightmap.png',
}).then(function (resources /*an object containing our keys with the loaded resources*/) {
    init(resources);

    render(0);
});


/**
 * initializes OpenGL context, compile shader, and load buffers
 */
function init(resources) {
    console.log("init webgl");

    //create a GL context
    gl = createContext(1024, 1024);

    //initRenderToTexture();

    gl.enable(gl.DEPTH_TEST);

    // Enable alpha blending
    gl.blendFuncSeparate(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA, gl.ONE, gl.ONE_MINUS_SRC_ALPHA);
    gl.enable(gl.BLEND);

    //create scenegraph
    root = createSceneGraph(gl, resources);

    initInteraction(gl.canvas);
}

function createSceneGraph(gl, resources) {
    //create scenegraph
    const root = new ShaderSGNode(createProgram(gl, resources.vs_single, resources.fs_single));

    root.append(new SetUniformSGNode("u_enableHeightTexture", false));
    root.append(new SetUniformSGNode("u_enableWooble", false));
    root.append(new SetUniformSGNode("u_enableObjectTexture", false));

    {
        // create sun
        sun = createSun(resources);
        root.append(sun);
    }

    {
        root.append(new TransformationSGNode(glm.translate(350, 400, 80), [createAnimatedLightNode(resources)]));
    }

    {
        let world = new World(root,resources);
        world.createWorld();
    }

    {
        cameraMovement = new CameraMovement(camera);

        cameraMovement.addNode(0, [863.4090576171875,254.03794860839844,34.05373001098633], -376, 91, "Heightmap");
        cameraMovement.addNode(1*1000, [863.4090576171875,254.03794860839844,34.05373001098633], -376, 110);
        cameraMovement.addNode(2*1000, [863.4090576171875,254.03794860839844,34.05373001098633], -261, 122);
        cameraMovement.addNode(5*1000, [805.6695556640625,238.8374786376953,76.76001739501953], -269, 117);
        cameraMovement.addNode(10*1000, [737.209228515625,231.30592346191406,110.50533294677734], -281, 98);
        cameraMovement.addNode(11*1000, [707.1976318359375,236.44525146484375,114.47303009033203], -265, 92, "Heightmap, LOD, Billboards");
        cameraMovement.addNode(12*1000, [704.8419189453125,259.50521850585938,137.6016845703125], -247, 75);
        cameraMovement.addNode(13*1000, [655.0079956054688,314.9285888671875,140.09701538085938], -194, 70);
        cameraMovement.addNode(14*1000, [434.55 , 201.17 , 198.71], 22, 65);
        cameraMovement.addNode(17*1000, [387.6448059082031,274.8894958496094,105.36354064941406], -346, 69);
        cameraMovement.addNode(18*1000, [306.32476806640625,336.2745056152344,88.91010284423828], -408, 72);
        cameraMovement.addNode(25*1000, [629.88 , 593.76 , 361.11], -614, 51);
        cameraMovement.addNode(30*1000, [1052.996337890625,506.744140625,672.3217163085938], -614, 34, "Heightmap, Billboards");
    }

    return root;
}

//camera control
function initInteraction(canvas) {
    camera.addMouseEventListeners(canvas);

    //register globally
    document.addEventListener('keypress', function(event) {
        switch (event.code) {
            case 'KeyR':
                showAnimation = false;
                camera.reset();
                break;
            case 'Digit1':
                showWireframe = !showWireframe;
                console.info("Enable Wireframe: " + showWireframe);
                break;
            case 'Digit2':
                cameraMovement.startAnimation();
                globalTimeInMilliseconds = 3 * 60 * 60 * 1000; // set time for animation
                showDayNight = true;
                showAnimation = true;
                console.info("Start Animation");
                break;
            case 'Digit3':
                showDayNight = !showDayNight;
                console.info("Show Day/Night Cycle: " + showDayNight);
                break;

            case 'KeyQ':
                    // root.append(tree.loadNewObjZAuto(195.63,239.46,3,0));
                    console.log("this.root.append(this.tree.loadNewObjZAuto(" + -camera.pos[0].toFixed(2) + " , " + -camera.pos[1].toFixed(2) + ",this.getRandomArbitrary(1,5),0));");
                break;

            case 'Digit0':
                console.log("cameraMovement.addNode(0, [" + -camera.pos[0].toFixed(2) + " , " + -camera.pos[1].toFixed(2) + " , " + -camera.pos[2].toFixed(2) + "], " + camera._rotation.x + ", " + camera._rotation.y + ");");
                break;
            default:
                break;
        }
    });
}

// we need to calculate a delta
var oldTimeInMilliseconds = 0;
var globalTimeInMilliseconds = 3 * 60 * 60 * 1000;

/**
 * render frames
 */
function render(timeInMilliseconds) {
    // calculate frame delta
    let delta = timeInMilliseconds - oldTimeInMilliseconds;
    oldTimeInMilliseconds = timeInMilliseconds;

    // update global time
    if (showDayNight) {
        const speedup = 2000; //2000;
        globalTimeInMilliseconds += delta*speedup;
    } else {
        globalTimeInMilliseconds = 10 * 60 * 60 * 1000;
    }

    //setup viewport
    gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);

    var attenuation = 1.;
    if(sun.direction[2] > 0.) {
        attenuation = Math.max(0.2, 1. - (sun.direction[2]*2)); // 0.1 / attenuation[2] + 0.1;
    }
    gl.clearColor(0.9 * attenuation, 1.0 * attenuation, 1.0 * attenuation, 1.0);

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    //setup context and camera matrices
    const context = createSGContext(gl);
    context.timeInMilliseconds = timeInMilliseconds;
    context.globalTimeInMilliseconds = globalTimeInMilliseconds;

    // update camera position
    updateCameraMovement(delta);
    if(showAnimation) {
        let finished = cameraMovement.updateCamera(context);

        if(finished) {
            console.info("Animation finished");
            showAnimation = false;
        }
    }

    context.projectionMatrix = mat4.perspective(mat4.create(), glm.deg2rad(60), gl.drawingBufferWidth / gl.drawingBufferHeight, 0.01, 3000);
    context.viewMatrix = camera.getViewMatrix();

    //get inverse view matrix to allow computing eye-to-light matrix
    context.invViewMatrix = mat4.invert(mat4.create(), context.viewMatrix);

    //render scenegraph
    root.render(context);

    //displayText(parseInt(1/(delta*0.001)) + " FPS" + "| X:" + camera.pos[0].toFixed(2)*-1 + " Y:" + camera.pos[1].toFixed(2)*-1 + " Z:" +camera.pos[2].toFixed(2)*-1 );

    //animate
    requestAnimationFrame(render);
}


function updateCameraMovement(delta) {
    const cameraMovementPerMs = 0.1;

    if(keys['KeyW']) {
        showAnimation = false;
        camera.moveForward(delta * cameraMovementPerMs);
    }
    if(keys['KeyS']) {
        showAnimation = false;
        camera.moveForward(delta * (-cameraMovementPerMs));
    }
    if(keys['KeyA']) {
        showAnimation = false;
        camera.moveSide(delta * cameraMovementPerMs);
    }
    if(keys['KeyD']) {
        showAnimation = false;
        camera.moveSide(delta * (-cameraMovementPerMs));
    }
}