/**
 * Our helper class to allow us to specify the camera path using nodes
 */
class CameraMovement {

    constructor(camera) {
        this.camera = camera;
        this.nodes = [];
        this.curNodeStart = 0;
        this.timeMsStart = -1;
    }

    addNode(timeMs, position, rot_x, rot_y, text) {
        let rotationMatrix = mat3.fromMat4(mat3.create(), mat4.multiply(mat4.create(),
            glm.rotateX(-rot_y),
            glm.rotateZ(-rot_x)));

        let view = quat.normalize(quat.create(), quat.fromMat3(quat.create(), rotationMatrix));
        let node_content = {'timeMs': timeMs,
            'position': [-position[0], -position[1], -position[2]],
            'view':view,
            'rotation_x': rot_x,
            'rotation_y': rot_y
        };
        if(text !== undefined) {
            node_content.text = text;
        }

        this.nodes.push(node_content);
    }

    startAnimation() {
        this.timeMsStart = -1;
        this.curNodeStart = 0;
    }

    updateCameraLinear(context) {
        let animationTime = context.timeInMilliseconds - this.timeMsStart;
        let node0 = this.nodes[this.curNodeStart];
        let node1 = this.nodes[this.curNodeStart + 1];
        let interpolationPoint = (animationTime - node0.timeMs) / ( node1.timeMs - node0.timeMs);

        if(interpolationPoint < 0) {
            return false; // wrong point
        } else if (interpolationPoint > 1) {
            this.curNodeStart += 1;
            console.info("go to next animation node: [" + this.curNodeStart + "]");
            // TODO: Workaround to have better camera alignment, does not work between nodes
            this.camera._rotation.x = node1.rotation_x;
            this.camera._rotation.y = node1.rotation_y;
            return this.updateCamera(context); // render next node
        } else if(interpolationPoint < 0.5 && node0.text !== undefined) {
            displayText(node0.text);
        } else if(node1.text !== undefined) {
            displayText(node1.text);
        }

        // do linear interpolation
        this.camera.pos = vec3.lerp(vec3.create(), node0.position, node1.position, interpolationPoint);
        this.camera.view = quat.slerp(quat.create(), node0.view, node1.view, interpolationPoint);

        return false;
    }

    updateCameraSpline(context) {
        let animationTime = context.timeInMilliseconds - this.timeMsStart;
        let node0 = this.nodes[this.curNodeStart];
        let node1 = this.nodes[this.curNodeStart + 1];
        let node2 = this.nodes[this.curNodeStart + 2];
        let node3 = this.nodes[this.curNodeStart + 3];
        let fullInterpolationPoint = (animationTime - node0.timeMs) / (node3.timeMs - node0.timeMs);

        // TODO: improve interpolation time position, based on distance of nodes

        if(fullInterpolationPoint < 0) {
            return false; // wrong point
        } else if (fullInterpolationPoint > 1) {
            this.curNodeStart += 3;
            console.info("go to next animation node: [" + this.curNodeStart + "]");
            // TODO: Workaround to have better camera alignment, does not work between nodes
            this.camera._rotation.x = node3.rotation_x;
            this.camera._rotation.y = node3.rotation_y;
            return this.updateCamera(context); // render next node
        } else if(fullInterpolationPoint < 0.25 && node0.text !== undefined) {
            displayText(node0.text);
        } else if(fullInterpolationPoint < 0.5 && node1.text !== undefined) {
            displayText(node1.text);
        } else if(fullInterpolationPoint < 0.75 && node2.text !== undefined) {
            displayText(node2.text);
        } else if(node3.text !== undefined) {
            displayText(node3.text);
        }

        // do spline interpolation
        this.camera.pos = vec3.bezier(vec3.create(), node0.position, node1.position, node2.position, node3.position, fullInterpolationPoint);
        this.camera.view = quat.sqlerp(quat.create(), node0.view, node1.view, node2.view, node3.view, fullInterpolationPoint);

        return false;
    }

    updateCamera(context) {
        if(this.timeMsStart === -1) {
            this.timeMsStart = context.timeInMilliseconds;
        }
        if(this.nodes.length > this.curNodeStart + 3) {
            return this.updateCameraSpline(context);
        } else if(this.nodes.length > this.curNodeStart + 1) {
            return this.updateCameraLinear(context);
        } else {
            return true;
        }
    }
}