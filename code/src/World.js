/**
 * Build our world
 */
class World {
    constructor(root, resources) {
        this.terrain = null;
        this.root = root;
        this.resources = resources;

    }

    createWorld() {

        {
            this.root.append(createTerrain(this.resources));
            this.terrain = terrain; // TODO: little hack
        }

        {
            //create houses
            this.placeHouses();
        }

        {
            //create forest
            let forest = new SGNode();
            this.createForest(forest);
            this.root.append(forest);
        }

        {
            //create grass
            let plantMaterial = new LoadMtlSGNode(this.resources.plant_mtl);
            let plantObj = createPlantObj(this.resources);
            this.createRandomObjects(plantMaterial, plantObj, 0, 0, 1000, 1000, 1, 5, 0, 360, 40, 270, 4, 1, 3);
            this.root.append(plantMaterial);
        }


        {
            //create mushrooms
            let mushroomMaterial = new LoadMtlSGNode(this.resources.mushroom1_mtl);
            let mushroomObj = createMushroomObj(this.resources);
            this.createRandomObjects(mushroomMaterial, mushroomObj, 0, 0, 1000, 1000, 1, 5, 0, 360, 40, 150, 4, 1, 1);
            this.root.append(mushroomMaterial);
        }

        //create grass
        {
            let grassMaterial = new LoadMtlSGNode(this.resources.grass_mtl);
            let grassObj = createGrassObj(this.resources);
            this.createRandomObjects(grassMaterial, grassObj, 0, 0, 1000, 1000, 1, 5, 0, 0, 40, 170, 2, 1, 1);
            this.root.append(grassMaterial);
        }

        {
            // some alpha texture
            this.root.append(new TransformationSGNode(glm.transform({translate: [673, 237, 125]}), [
                createHyBillboard(this.resources)
            ]));
        }
    }

    placeHouses() {
        let houseObj = createHouse(this.resources);
        // place house
        this.root.append(
            new TransformationSGNode(glm.transform({translate:[305, 435, 44], rotateZ:-25}),
                houseObj
            )
        );
        this.root.append(
            new TransformationSGNode(glm.transform({translate:[410, 430, 48], rotateZ:295}),
                houseObj
            )
        );

        let lighthouseObj = createLighthouse(this.resources);
        // place lighthouse
        this.root.append(new TransformationSGNode(glm.translate(940, 480, 10), lighthouseObj));
    }

    /**
     * @param node, there new nodes are appended
     * @param obj object which is generated
     * @param bx begin x
     * @param by begin y
     * @param ex end x
     * @param ey end y
     * @param mins min scale
     * @param maxs max scale
     * @param minr min rotation
     * @param maxr max rotation
     * @param minh min height
     * @param maxh max height
     * @param i interval
     * @param minabs min distance
     * @param heightOffset
     */
    createRandomObjects(node, obj, bx, by, ex, ey, mins, maxs, minr, maxr, minh, maxh, i, minAbs, heightOffset) {
        let randx = bx;
        let c = 0;

        while (randx <= ex) {
            c++;
            randx += this.getRandomArbitrary(minAbs, i);
            let randy = this.getRandomArbitrary(by, ey);

            let height = this.terrain.getZPos(randx, randy);
            if (height < minh || height > maxh || (height <= 57 && height >= 35))
                continue;

            let s = this.getRandomArbitrary(mins, maxs);

            let r = this.getRandomArbitrary(minr, maxr);

            node.append(new TransformationSGNode(glm.translate(randx, randy,  height + heightOffset),
                new TransformationSGNode(glm.scale(s, s, s),
                    new TransformationSGNode(glm.rotateZ(r),
                        obj
                    )
                )
            ));


        }

        console.log("Created: " + c + " Objects");
    }


    createForest(fn) {
        this.treeObj = createTreeObj(this.resources); // create tree object only once

        let node = new LoadMtlSGNode(this.resources.tree3_mtl);
        let tmp = new WoobleSGNode(//new TextureSGNode(this.resources.tree3_texture,
            node);//);

        tmp.woobleHeight = 30.;
        tmp.woobleAmplitude = [10, 1., -10];

        node.append(this.createTreeNodeAtPosition(256.89, 397.53));
        node.append(this.createTreeNodeAtPosition(243.7, 393.87));
        node.append(this.createTreeNodeAtPosition(240.52, 361.74));
        node.append(this.createTreeNodeAtPosition(262.86, 339.98));
        node.append(this.createTreeNodeAtPosition(277.9, 309.19));
        node.append(this.createTreeNodeAtPosition(277.18, 267.88));
        node.append(this.createTreeNodeAtPosition(297.31, 232.64));
        node.append(this.createTreeNodeAtPosition(323.39, 242.74));
        node.append(this.createTreeNodeAtPosition(351.55, 241.87));
        node.append(this.createTreeNodeAtPosition(367.31, 214.21));
        node.append(this.createTreeNodeAtPosition(392.72, 196.97));
        node.append(this.createTreeNodeAtPosition(419.49, 186.15));
        node.append(this.createTreeNodeAtPosition(459.05, 170.17));
        node.append(this.createTreeNodeAtPosition(491.73, 157.25));
        node.append(this.createTreeNodeAtPosition(510.21, 179.06));
        node.append(this.createTreeNodeAtPosition(516.33, 204.54));
        node.append(this.createTreeNodeAtPosition(519.46, 217.35));
        node.append(this.createTreeNodeAtPosition(548.57, 217.02));
        node.append(this.createTreeNodeAtPosition(564.83, 197.54));
        node.append(this.createTreeNodeAtPosition(577.92, 179.52));
        node.append(this.createTreeNodeAtPosition(595.01, 167.11));
        node.append(this.createTreeNodeAtPosition(613.18, 157.9));
        node.append(this.createTreeNodeAtPosition(633.44, 167.2));
        node.append(this.createTreeNodeAtPosition(637.07, 187.77));
        node.append(this.createTreeNodeAtPosition(631.4, 213.34));
        node.append(this.createTreeNodeAtPosition(652.12, 222.21));
        node.append(this.createTreeNodeAtPosition(661.89, 221.07));
        node.append(this.createTreeNodeAtPosition(686.39, 233.42));
        node.append(this.createTreeNodeAtPosition(706.16, 233.77));
        node.append(this.createTreeNodeAtPosition(727.19, 244.43));
        node.append(this.createTreeNodeAtPosition(747.96, 242.24));
        node.append(this.createTreeNodeAtPosition(768.54, 249.16));
        node.append(this.createTreeNodeAtPosition(782.31, 252.09));
        node.append(this.createTreeNodeAtPosition(812.99, 230.28));
        node.append(this.createTreeNodeAtPosition(836.4, 206.63));
        node.append(this.createTreeNodeAtPosition(851.24, 177.22));
        node.append(this.createTreeNodeAtPosition(263.04, 320.61));
        node.append(this.createTreeNodeAtPosition(284.27, 264.49));
        node.append(this.createTreeNodeAtPosition(310.93, 244.98));
        node.append(this.createTreeNodeAtPosition(340.85, 225.54));
        node.append(this.createTreeNodeAtPosition(415.02, 233.92));
        node.append(this.createTreeNodeAtPosition(461.83, 210.46));
        node.append(this.createTreeNodeAtPosition(524.07, 188.84));
        node.append(this.createTreeNodeAtPosition(571.75, 209.14));
        node.append(this.createTreeNodeAtPosition(657.6, 203.04));
        node.append(this.createTreeNodeAtPosition(701.98, 218.69));
        node.append(this.createTreeNodeAtPosition(765.5, 266.48));
        node.append(this.createTreeNodeAtPosition(807.45, 278.32));
        node.append(this.createTreeNodeAtPosition(815.35, 323.16));
        node.append(this.createTreeNodeAtPosition(838.8, 362.5));
        node.append(this.createTreeNodeAtPosition(850.39, 396.47));
        node.append(this.createTreeNodeAtPosition(875.42, 409.66));
        node.append(this.createTreeNodeAtPosition(839.78, 406.36));
        node.append(this.createTreeNodeAtPosition(841.42, 259.52));
        node.append(this.createTreeNodeAtPosition(734.59, 200.62));
        node.append(this.createTreeNodeAtPosition(856.95, 355.09));
        node.append(this.createTreeNodeAtPosition(821.94, 343.47));
        node.append(this.createTreeNodeAtPosition(829.51, 165));
        node.append(this.createTreeNodeAtPosition(840.38, 119.1));
        node.append(this.createTreeNodeAtPosition(577.61, 104.12));
        node.append(this.createTreeNodeAtPosition(340.43, 505.19));
        fn.append(tmp);

    }

    createTreeNodeAtPosition(x, y) {
        let rs = this.getRandomArbitrary(1, 5);
        let randomRotate = this.getRandomArbitrary(0, 360);

        return new TransformationSGNode(glm.translate(x,y,terrain.getZPos(x,y)),
            new TransformationSGNode(glm.scale(rs,rs,rs),
                new TransformationSGNode(glm.rotateZ(randomRotate), this.treeObj)
            )
        );
    }

    getRandomArbitrary(min, max) {
        let v = Math.random() * (max - min) + min;
        return v;
    }

}