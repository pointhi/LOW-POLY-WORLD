/**
 * Handles the camera and it's movement to the scene
 *
 * @class Camera
 * @name Camera
 */
class Camera {
    constructor() {
        this.reset();
    }

    reset() {
        this.pos = vec3.fromValues(-450, -600, -400); // TODO: default values
        this._rotation = {
            x: -150,
            y: 25
        };

        this._updateView();
    }

    getWorldPosition() {
        return vec3.negate(vec3.create(), this.pos);
    }

    _updateView() {
        let rotationMatrix = mat3.fromMat4(mat3.create(), mat4.multiply(mat4.create(),
            glm.rotateX(-this._rotation.y),
            glm.rotateZ(-this._rotation.x)));

        this.view = quat.normalize(quat.create(), quat.fromMat3(quat.create(), rotationMatrix));
    }

    applyMouseRotation(delta_x, delta_y) {
        // rotate x
        this._rotation.x = (this._rotation.x + delta_x + 360) % 360;

        // rotate y
        if(this._rotation.y + delta_y < 180 && this._rotation.y + delta_y > 0) {
            this._rotation.y += delta_y;
        } else if(this._rotation.y + delta_y >= 180) {
            this._rotation.y = 180;
        } else if(this._rotation.y + delta_y <= 0) {
            this._rotation.y = 0;
        }

        // update view matrix
        this._updateView();
    }

    getViewMatrix() {
        let mouseRotateMatrix = mat4.fromQuat(mat4.create(), camera.view);
        let translationMatrix = mat4.fromTranslation(mat4.create(), camera.pos);
        return mat4.multiply(mat4.create(), mouseRotateMatrix, translationMatrix);
    }

    moveForward(delta) {
        let moveVector = vec3.scale(vec3.create(), this.getFront(), delta);
        this.pos = vec3.add(vec3.create(), this.pos, moveVector);
    }

    moveSide(delta) {
        let moveVector = vec3.scale(vec3.create(), this.getSide(), delta);
        this.pos = vec3.add(vec3.create(), this.pos, moveVector);
    }

    moveUp(delta) {
        let moveVector = vec3.scale(vec3.create(), this.getUp(), delta);
        this.pos = vec3.add(vec3.create(), this.pos, moveVector);
    }

    getFront() {
        // https://stackoverflow.com/questions/18306228/how-to-control-a-3d-camera-using-a-single-quaternion-representation-for-its-orie
        let orientation = mat4.fromQuat(mat4.create(), quat.normalize(quat.create(), this.view));
        let front = vec3.fromValues(orientation[2], orientation[6], orientation[10]);
        return vec3.normalize(vec3.create(), front);
    }

    getSide() {
        let orientation = mat4.fromQuat(mat4.create(), quat.normalize(quat.create(), this.view));
        let front = vec3.fromValues(orientation[0], orientation[4], orientation[8]);
        return vec3.normalize(vec3.create(), front);
    }

    getUp() {
        let orientation = mat4.fromQuat(mat4.create(), quat.normalize(quat.create(), this.view));
        let front = vec3.fromValues(orientation[1], orientation[5], orientation[7]);
        return vec3.normalize(vec3.create(), front);
    }

    addMouseEventListeners(canvas) {
        const thisCamera = this; // required to register interacting event handler

        const mouse = {
            pos: { x : 0, y : 0},
            leftButtonDown: false
        };
        function toPos(event) {
            //convert to local coordinates
            const rect = canvas.getBoundingClientRect();
            return {
                x: event.clientX - rect.left,
                y: event.clientY - rect.top
            };
        }
        canvas.addEventListener('mousedown', function(event) {
            mouse.pos = toPos(event);
            mouse.leftButtonDown = event.button === 0;
        });
        canvas.addEventListener('mousemove', function(event) {
            const pos = toPos(event);
            const delta = { x : mouse.pos.x - pos.x, y: mouse.pos.y - pos.y };
            if (mouse.leftButtonDown) {
                //add the relative movement of the mouse to the rotation variables
                thisCamera.applyMouseRotation(delta.x, delta.y);
            }
            mouse.pos = pos;
        });
        canvas.addEventListener('mouseup', function(event) {
            mouse.pos = toPos(event);
            mouse.leftButtonDown = false;
        });
    }
}