/**
 * Created by thomas on 18.06.17.
 */

class OurAdvancedTextureSGNode extends AdvancedTextureSGNode {
    constructor(image, children ) {
        super(image, children )
    }

    render(context) {
        if (this.textureId < 0) {
            this.init(context.gl);
        }
        //set additional shader parameters
        gl.uniform1i(gl.getUniformLocation(context.shader, this.uniform), this.textureunit);

        //activate and bind texture
        gl.activeTexture(gl.TEXTURE0 + this.textureunit);
        gl.bindTexture(gl.TEXTURE_2D, this.textureId);

        //render all children
        this.children.forEach(function (c) {
            return c.render(context);
        });

        /*
         * we do not cleanup, because we have a shader where texturing is optional!
         *
         * Otherwise we get an error like:
         *
         * <WARNING: there is no texture bound to the unit 4>
         *
         * This is not a warning which shows errors in our code (because we have a uniform which
         * disables using the texture, but some requirement by OpenGL). By leaving the texture on
         * the gpu we circumvent his error.
         */
    }
}