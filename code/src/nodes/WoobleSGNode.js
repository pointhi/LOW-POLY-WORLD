/**
 * Enables Wooble for children
 */
class WoobleSGNode extends SGNode{
    constructor(children) {
        super(children);

        this.wooblePeriod = 3000.;
        this.woobleHeight = 10.; // TODO: better calculation, uses sceneMatrix for translation
        this.woobleAmplitude = [1., 1., 0.];

        this.uniform = 'u_wooble';
    }

    setTransformationUniforms(context) {
        gl.uniform1f(gl.getUniformLocation(context.shader, this.uniform+'Time'), context.timeInMilliseconds);
        gl.uniform1f(gl.getUniformLocation(context.shader, this.uniform+'Periode'), this.wooblePeriod);
        gl.uniform1f(gl.getUniformLocation(context.shader, this.uniform+'Height'), this.woobleHeight);
        gl.uniform3fv(gl.getUniformLocation(context.shader, this.uniform+'Amplitude'), this.woobleAmplitude);
    }

    render(context) {
        this.setTransformationUniforms(context);

        // enable wooble for it's children
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableWooble'), 1);

        // render children
        super.render(context);

        // disable wooble
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableWooble'), 0);
    }
}