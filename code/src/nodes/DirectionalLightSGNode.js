/**
 * Create a Directional Light
 */
class DirectionalLightSGNode extends SGNode {
    constructor(children) {
        super(children);

        this.ambient = [0, 0, 0, 1];
        this.diffuse = [1, 1, 1, 1];
        this.specular = [1, 1, 1, 1];
        this.direction = [1, 0, -1];
        this.attenuation = 1;

        //uniform name
        this.uniform = 'u_dirlight';

        this._worldDirection = vec3.create();
    }

    setLightUniforms(context) {
        const gl = context.gl;
        //no materials in use
        if (!context.shader || !isValidUniformLocation(gl.getUniformLocation(context.shader, this.uniform+'.ambient'))) {
            return;
        }

        // when the light goes under the horizon, we don't wan't it to light our scene from below.
        let ambient = vec4.scale(vec4.create(), this.ambient, Math.max(this.attenuation, 0.5)); // TODO: add global light
        let diffuse = vec4.scale(vec4.create(), this.diffuse, this.attenuation);
        let specular = vec4.scale(vec4.create(), this.specular, this.attenuation);

        gl.uniform4fv(gl.getUniformLocation(context.shader, this.uniform+'.ambient'), ambient);
        gl.uniform4fv(gl.getUniformLocation(context.shader, this.uniform+'.diffuse'), diffuse);
        gl.uniform4fv(gl.getUniformLocation(context.shader, this.uniform+'.specular'), specular);
    }

    setLightDirection(context) {
        const gl = context.gl;
        if (!context.shader || !isValidUniformLocation(gl.getUniformLocation(context.shader, this.uniform+'Dir'))) {
            return;
        }
        const direction = this._worldDirection || this.direction;
        gl.uniform3f(gl.getUniformLocation(context.shader, this.uniform+'Dir'), direction[0], direction[1], direction[2]);
    }

    computeLightDirection(context) {
        //transform with the current model view matrix
        const modelViewMatrix = mat4.multiply(mat4.create(), context.viewMatrix, context.sceneMatrix);
        const normalMatrix = mat3.normalFromMat4(mat3.create(), modelViewMatrix);

        //
        this._worldDirection = vec3.transformMat3(this._worldDirection, this.direction, normalMatrix);
    }

    render(context) {
        // We also need to compute the light direction
        this.computeLightDirection(context);

        this.setLightUniforms(context);
        this.setLightDirection(context);

        //render children
        super.render(context);
    }
}