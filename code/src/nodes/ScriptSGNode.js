/**
 * Executes a function before entering the children
 */
class ScriptSGNode extends SGNode {
    constructor(script, children) {
        super(children);
        this.script = script;
    }

    render(context) {
        // executes function with current context
        this.script(context);

        //render children
        super.render(context);
    }
}