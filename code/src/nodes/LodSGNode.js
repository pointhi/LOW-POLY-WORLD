/**
 * Handles Level of detail, based on a distance and an arbitrary number of LOD steps
 */
class LodSGNode extends SGNode {
    constructor(distance, children) {
        super([]);
        this.distance = distance;
        this.center = [0, 0, 0];
        this.lodChildren = children;
    }

    appendLodChild(child) {
        this.lodChildren.push(child);
    }

    render(context) {
        if(this.lodChildren.length > 0) {
            // calculate center of LOD node
            let lodCenter = vec3.transformMat4(vec3.create(), this.center, context.sceneMatrix);

            // calculate distance ot camera, use square distance to save computing power
            let distance = vec3.squaredDistance(lodCenter, camera.getWorldPosition());

            // calculate level based on distance
            let level = Math.min(this.lodChildren.length - 1, parseInt(distance / (this.distance * this.distance)));

            // render children based on LOD
            let child = this.lodChildren[level];

            if (child !== null) {
                child.render(context);
            }
        }

        // render normal children without usage of LOD
        super.render(context);
    }
}