/**
 * Create a billboard, based on the inverse of the modelView Matrix
 */
class BillboardSGNode extends TransformationSGNode {

    constructor(child) {
        super(null, child);
        this.scaleBillboard = true;
    }

    render(context) {
        //this.matrix =  mat4.fromQuat(mat4.create(), camera.view);
        //let center = vec3.transformMat4(vec3.create(), vec3.create(), context.sceneMatrix);
        //let look_vektor = vec3.normalize(vec3.create(), vec3.subtract(vec3.create(), camera.getWorldPosition(), center));

        // @see: https://swiftcoder.wordpress.com/2008/11/25/constructing-a-billboard-matrix/
        // TODO: no perfect billboard, but work's for now

        const modelViewMatrix = mat4.multiply(mat4.create(), context.viewMatrix, context.sceneMatrix);
        let invViewMatrix = mat4.invert(mat4.create(), modelViewMatrix);

        invViewMatrix[12] = 0.;
        invViewMatrix[13] = 0.;
        invViewMatrix[14] = 0.;

        // @see: https://math.stackexchange.com/questions/237369/given-this-transformation-matrix-how-do-i-decompose-it-into-translation-rotati
        if(this.scaleBillboard) {
            const sx = vec3.length([context.sceneMatrix[0], context.sceneMatrix[4], context.sceneMatrix[8]]);
            const sy = vec3.length([context.sceneMatrix[1], context.sceneMatrix[5], context.sceneMatrix[9]]);
            const sz = vec3.length([context.sceneMatrix[2], context.sceneMatrix[6], context.sceneMatrix[10]]);
            const scale = [sx, sy, sz];
            invViewMatrix = mat4.scale(mat4.create, invViewMatrix, scale);
        }

        // TODO: correct the x,y,z axis
        this.matrix = invViewMatrix;

        super.render(context);
    }
}
