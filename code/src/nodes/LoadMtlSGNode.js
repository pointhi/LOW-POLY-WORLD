/**
 * Parse a mtl file, and use it's material parameter as material properties
 */
class LoadMtlSGNode extends MaterialSGNode {
    constructor( mtl ,children) {
        super(children);
        this.mtl = mtl;

        if(this.mtl != null) {
            let material = parseMtlFile(this.mtl);

            //using default blender mtl. index
            this.ambient = material["Material.001"].ambient;
            this.diffuse = material["Material.001"].diffuse;
            this.specular = material["Material.001"].specular;
            this.shininess = material["Material.001"].shininess;
            this.shininess = material["Material.001"].emission;
        }
    }

    setMaterialUniforms(context) {
        if(this.mtl != null){
            super.setMaterialUniforms(context);
        }
    }
}