/**
 * Create a Spot Light
 */
class SpotLightSGNode extends LightSGNode {
    constructor(children) {
        super(children);

        this.direction = [1, 0, -0.2];
        this.angle = 45./2.;

        //uniform name
        this.uniform = 'u_spotlight';

        this._worldDirection = vec3.create();
    }

    setLightUniforms(context) {
        super.setLightUniforms(context);

        const gl = context.gl;
        //no materials in use
        if (!context.shader || !isValidUniformLocation(gl.getUniformLocation(context.shader, this.uniform+'.ambient'))) {
            return;
        }
        gl.uniform1f(gl.getUniformLocation(context.shader, this.uniform+'Angle'), this.angle);
    }

    setLightDirection(context) {
        const gl = context.gl;
        if (!context.shader || !isValidUniformLocation(gl.getUniformLocation(context.shader, this.uniform+'Dir'))) {
            return;
        }
        const direction = this._worldDirection || this.direction;
        gl.uniform3f(gl.getUniformLocation(context.shader, this.uniform+'Dir'), direction[0], direction[1], direction[2]);
    }

    computeLightDirection(context) {
        //transform with the current model view matrix
        const modelViewMatrix = mat4.multiply(mat4.create(), context.viewMatrix, context.sceneMatrix);
        const normalMatrix = mat3.normalFromMat4(mat3.create(), modelViewMatrix);

        //
        this._worldDirection = vec3.transformMat3(this._worldDirection, this.direction, normalMatrix);
    }

    setLight(context) {
        super.setLight(context);
        this.setLightDirection(context);
    }

    render(context) {
        // We also need to compute the light direction
        this.computeLightDirection(context);

        //render children
        super.render(context);
    }
}