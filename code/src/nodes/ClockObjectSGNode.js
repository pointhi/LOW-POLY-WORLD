/**
 * Builds an animated clock
 */
class ClockObjectSGSGNode extends SGNode {
    constructor(resources, children) {
        super(children);

        let clockBody = new MaterialSGNode(new TransformationSGNode(glm.transform({translate: [0, 0, -0.2], scale: [1,1,0.1]}), new RenderSGNode(createCube(1))));
        this.secondsPointer = new TransformationSGNode(mat4.create(), new RenderSGNode(createArrow(1, 0.1, 0.05)));
        this.minutesPointer = new TransformationSGNode(mat4.create(), new RenderSGNode(createArrow(0.9, 0.1, 0.05)));
        this.hoursPointer = new TransformationSGNode(mat4.create(), new RenderSGNode(createArrow(0.8, 0.15, 0.05)));

        clockBody.ambient = [0.5, 0.5, 0.5, 1];
        clockBody.diffuse = [0.0, 0.8, 0, 1];
        clockBody.specular = [0.4, 0.4, 0.4, 1];
        clockBody.shininess = 2.0;
        this.append(clockBody);

        let arrowMaterial = new MaterialSGNode(new TextureSGNode(resources.arrow , [
            //this.secondsPointer, // with our speed, this pointer doesn't look nice in the scene
            this.minutesPointer,
            this.hoursPointer
        ]));

        arrowMaterial.ambient = [0.5, 0.5, 0.5, 1];
        arrowMaterial.diffuse = [0.3, 0.8, 0, 1];
        arrowMaterial.specular = [0.8, 0.8, 0.8, 1];
        arrowMaterial.emission = [0.1, 0.1, 0.1, 1];
        arrowMaterial.shininess = 100.0;

        this.append(arrowMaterial);
    }

    setSeconds(seconds) {
        let angle = -(seconds * 360. / 60.);
        this.secondsPointer.matrix = glm.transform({translate: [0,0,0.1], rotateZ: angle});
    }

    setMinutes(minutes) {
        let angle = -(minutes * 360. / 60.);
        this.minutesPointer.matrix = glm.transform({translate: [0,0,0.05], rotateZ: angle});
    }

    setHours(hour){
        let angle = -(hour * 360. / 24.);
        this.hoursPointer.matrix = glm.transform({ rotateZ: angle});
    }

    render(context) {
        this.setSeconds(parseInt((context.globalTimeInMilliseconds/1000) % 60));
        this.setMinutes((context.globalTimeInMilliseconds/1000/60) % 60);
        this.setHours((context.globalTimeInMilliseconds/1000/60/60) % 24);

        super.render(context);
    }
}