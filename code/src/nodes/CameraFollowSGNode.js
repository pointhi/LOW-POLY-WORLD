/**
 * Builds a scene matrix centered to the camera position without rotation part
 */
class CameraFollowSGNode extends SGNode {
    constructor(children) {
        super(children);
    }

    render(context) {
        //backup previous one
        var previous = context.sceneMatrix;

        context.sceneMatrix = mat4.fromTranslation(mat4.create(), camera.getWorldPosition());

        //render children
        super.render(context);

        //restore backup
        context.sceneMatrix = previous;
    }
}