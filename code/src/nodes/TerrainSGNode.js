var imageData; // TODO: move into terrain node

/**
 * Create a terrain from a heightmap including LOD and geometry gap fixes
 */
class TerrainSGNode extends SGNode {

    constructor(size, blocksize, children) {
        super(children);
        this.maxLOD = 3;
        this.size = size;
        this.blocksize = blocksize;
        this._initBuffer(blocksize);

        //for global variable!
        imageData = getImageData();
    }

    _initBuffer(size) {
        let localVertices = [];
        for(var x = 0; x <= size; x++) {
            for(var y = 0; y <= size; y++) {
                localVertices.push(x);
                localVertices.push(y);
                localVertices.push(0);
            }
        }

        this.vertices = new Float32Array(localVertices);

        this.vertexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, this.vertices, gl.STATIC_DRAW);

        let localNormals = [];
        for(var x = 0; x <= size; x++) {
            for(var y = 0; y <= size; y++) {
                localNormals.push(0);
                localNormals.push(0);
                localNormals.push(1);
            }
        }

        this.normals = new Float32Array(localNormals);

        this.normalBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.normalBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, this.normals, gl.STATIC_DRAW);

        let localTexCoord = [];
        for(var x = 0; x <= size; x++) {
            for(var y = 0; y <= size; y++) {
                localTexCoord.push(0);
                localTexCoord.push(0);
            }
        }

        this.texCoords = new Float32Array(localTexCoord);

        this.textureBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.textureBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, this.texCoords, gl.STATIC_DRAW);

        const maxLod = Math.min(this.maxLOD, parseInt(Math.sqrt(this.blocksize)));
        this.indexes = this.createIndexes(maxLod, -1);
        this.leftIndexes = this.createIndexes(maxLod, 0);
        this.downIndexes = this.createIndexes(maxLod, 1);
        this.rightIndexes = this.createIndexes(maxLod, 2);
        this.upIndexes = this.createIndexes(maxLod, 3);

        this.indexBuffer = this.createIndexBuffer(this.indexes);
        this.leftIndexBuffer = this.createIndexBuffer(this.leftIndexes);
        this.downIndexBuffer = this.createIndexBuffer(this.downIndexes);
        this.rightIndexBuffer = this.createIndexBuffer(this.rightIndexes);
        this.upIndexBuffer = this.createIndexBuffer(this.upIndexes);
    }


    createIndexes(maxLod, side) {
        let indexes = [];
        for(let i = 0; i < maxLod; i++) {
            indexes[i] = this._calculateIndexes(i, side);
        }
        return indexes;
    }

    createIndexBuffer(indexes) {
        let indexBuffer = [];
        for(let i = 0; i < indexes.length; i++) {
            indexBuffer[i] = gl.createBuffer();
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer[i]);
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indexes[i], gl.STATIC_DRAW);
        }
        return indexBuffer;
    }

    _calculateIndexes(level, higherLodSide) {
        const inc = parseInt(Math.pow(2, level));
        const inc2 = parseInt(inc / 2);
        const size = this.blocksize;
        let localIndexesL0 = [];

        /*
         * check if we calculate a tile which has a tile with
         *
         * -1 == none
         * 0 = right
         * 1 = upper
         * 2 = left
         * 3 = lower
         */

        let x_start = 0;
        let x_end = size;
        let y_start = 0;
        let y_end = size;
        switch(higherLodSide) {
            case 0:
                x_end -= inc;
                break;
            case 1:
                y_end -= inc;
                break;
            case 2:
                x_start += inc;
                break;
            case 3:
                y_start += inc;
                break;
            default:
            case -1:
                // use default values
                break;
        }

        if(higherLodSide === 2) {
            const x = 0;
            for(let y = y_start; y < y_end; y+=inc) {
                const i = x * (size + 1) + y;
                localIndexesL0.push(i + inc2);
                localIndexesL0.push(i);
                localIndexesL0.push(i + inc * (size + 1));
                localIndexesL0.push(i + inc2);
                localIndexesL0.push(i + inc * (size + 1));
                localIndexesL0.push(i + inc * (size + 1) + inc);
                localIndexesL0.push(i + inc2);
                localIndexesL0.push(i + inc);
                localIndexesL0.push(i + inc * (size + 1) + inc);
            }
        }

        for(let x = x_start; x < x_end; x+=inc) {
            if(higherLodSide === 1) {
                if((x / inc) % 2) {
                    const i = x * (size + 1) + size - inc;
                    localIndexesL0.push(i);
                    localIndexesL0.push(i + inc);
                    localIndexesL0.push(i + inc2 * (size + 1) + inc);
                    localIndexesL0.push(i);
                    localIndexesL0.push(i + inc * size + inc);
                    localIndexesL0.push(i + inc2 * (size + 1) + inc);
                    localIndexesL0.push(i + inc * size + 2 * inc);
                    localIndexesL0.push(i + inc * size + inc);
                    localIndexesL0.push(i + inc2 * (size + 1) + inc);
                }
            } else if(higherLodSide === 3) {
                if(!((x / inc) % 2)) {
                    const i = x * (size + 1);
                    localIndexesL0.push(i + inc2 * (size + 1));
                    localIndexesL0.push(i);
                    localIndexesL0.push(i + inc);
                    localIndexesL0.push(i + inc2 * (size + 1));
                    localIndexesL0.push(i + inc * (size + 1));
                    localIndexesL0.push(i + inc * (size + 1) + inc);
                    localIndexesL0.push(i + inc2 * (size + 1));
                    localIndexesL0.push(i + inc * (size + 1) + inc);
                    localIndexesL0.push(i + inc);
                }
            }

            for(let y = y_start; y < y_end; y+=inc) {
                /*
                 * push the nodes in this specific order, to allow us to use LINE_STRIP
                 * for debug rendering without artifacts.
                 */
                if((x / inc) % 2) {
                    const i = x * (size + 1) + (y_end - y) + y_start;
                    localIndexesL0.push(i);
                    localIndexesL0.push(i + inc * (size + 1));
                    localIndexesL0.push(i - inc);
                    localIndexesL0.push(i + inc * (size + 1) - inc);
                    localIndexesL0.push(i + inc * (size + 1));
                    localIndexesL0.push(i - inc);
                } else {
                    const i = x * (size + 1) + y;
                    localIndexesL0.push(i);
                    localIndexesL0.push(i + inc * size + inc);
                    localIndexesL0.push(i + inc * size + 2 * inc);
                    localIndexesL0.push(i);
                    localIndexesL0.push(i + inc);
                    localIndexesL0.push(i + inc * size + 2 * inc);
                }
            }

            if(higherLodSide === 1) {
                if(!((x / inc) % 2)) {
                    const i = x * (size + 1) + y_end;
                    localIndexesL0.push(i);
                    localIndexesL0.push(i + inc);
                    localIndexesL0.push(i + inc2 * (size + 1) + inc);
                    localIndexesL0.push(i);
                    localIndexesL0.push(i + inc * size + inc);
                    localIndexesL0.push(i + inc2 * (size + 1) + inc);
                    localIndexesL0.push(i + inc * size + 2 * inc);
                    localIndexesL0.push(i + inc * size + inc);
                    localIndexesL0.push(i + inc2 * (size + 1) + inc);
                }
            } else if(higherLodSide === 3) {
                if((x / inc) % 2) {
                    const i = x * (size + 1);
                    localIndexesL0.push(i + inc);
                    localIndexesL0.push(i);
                    localIndexesL0.push(i + inc2 * (size + 1));
                    localIndexesL0.push(i + inc);
                    localIndexesL0.push(i + inc * (size + 1) + inc);
                    localIndexesL0.push(i + inc2 * (size + 1));
                    localIndexesL0.push(i + inc * (size + 1) + inc);
                    localIndexesL0.push(i + inc * (size + 1));
                    localIndexesL0.push(i + inc2 * (size + 1));
                }
            }
        }
        if(higherLodSide === 0) {
            const x = x_end;
            for(let y = y_start; y < y_end; y+=inc) {
                const i = x * (size + 1) + (y_end - y) + y_start;
                localIndexesL0.push(i);
                localIndexesL0.push(i + inc * (size + 1));
                localIndexesL0.push(i + inc * (size + 1) - inc2);
                localIndexesL0.push(i + inc * (size + 1) - inc2);
                localIndexesL0.push(i);
                localIndexesL0.push(i - inc);
                localIndexesL0.push(i + inc * (size + 1) - inc2);
                localIndexesL0.push(i + inc * (size + 1) - inc);
                localIndexesL0.push(i - inc);
            }
        }

        return new Uint16Array(localIndexesL0);
    }

    setTransformationUniforms(context) {
        //set matrix uniforms
        const modelViewMatrix = mat4.multiply(mat4.create(), context.viewMatrix, context.sceneMatrix);
        const normalMatrix = mat3.normalFromMat4(mat3.create(), modelViewMatrix);
        const projectionMatrix = context.projectionMatrix;

        const gl = context.gl;
        const shader = context.shader;
        gl.uniformMatrix4fv(gl.getUniformLocation(shader, 'u_modelView'), false, modelViewMatrix);
        gl.uniformMatrix3fv(gl.getUniformLocation(shader, 'u_normalMatrix'), false, normalMatrix);
        gl.uniformMatrix4fv(gl.getUniformLocation(shader, 'u_projection'), false, projectionMatrix);
    }

    renderBlock(context, x, y, indexes, buffer) {
        if(x < 0 || x >= this.size || y < 0 || y >= this.size) {
            return
        }

        gl.uniform2fv(gl.getUniformLocation(context.shader, 'u_HeightMatrixDisplacement'), [x, y]);

        // only bind new buffer, if old one is the wrong one
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer);

        if (showWireframe) {
            // Old debugging code, which is based on single triangle rendering
            /*for (let i = 0; i < indexes.length / 3; i++) {
                gl.drawElements(gl.LINE_LOOP, 3, gl.UNSIGNED_SHORT, i * 6); //LINE_STRIP
            }*/
            gl.drawElements(gl.LINE_STRIP, indexes.length, gl.UNSIGNED_SHORT, 0);
        } else {
            gl.drawElements(gl.TRIANGLES, indexes.length, gl.UNSIGNED_SHORT, 0);
        }
    }

    renderBlockQuader(context, xleft, ydown, xright, yup, level, innerLevel) {
        // TODO: test against double rendering of nodes
        if(level === innerLevel) {
            const curIndexes = this.indexes[level];
            const curIndexBuffer = this.indexBuffer[level];
            for(let x = Math.max(0, xleft); x <= Math.min(xright, this.size-this.blocksize); x += this.blocksize) {
                this.renderBlock(context, x, yup, curIndexes, curIndexBuffer);
                this.renderBlock(context, x, ydown, curIndexes, curIndexBuffer);
            }
            for(let y = Math.max(ydown + this.blocksize); y < Math.min(yup, this.size); y += this.blocksize) {
                this.renderBlock(context, xleft, y, curIndexes, curIndexBuffer);
                this.renderBlock(context, xright, y, curIndexes, curIndexBuffer);
            }
        } else {
            const curCornerIndexes = this.indexes[level];
            const curCornerIndexBuffer = this.indexBuffer[level];

            this.renderBlock(context, xleft, ydown, curCornerIndexes, curCornerIndexBuffer);
            this.renderBlock(context, xleft, yup, curCornerIndexes, curCornerIndexBuffer);
            this.renderBlock(context, xright, ydown, curCornerIndexes, curCornerIndexBuffer);
            this.renderBlock(context, xright, yup, curCornerIndexes, curCornerIndexBuffer);

            if (xleft !== xright) {
                for(let y = ydown + this.blocksize; y < yup; y += this.blocksize) {
                    this.renderBlock(context, xright, y, this.rightIndexes[level], this.rightIndexBuffer[level]);
                    this.renderBlock(context, xleft, y, this.leftIndexes[level], this.leftIndexBuffer[level]);
                }
            }

            if (ydown !== yup) {
                for(let x= xleft + this.blocksize; x < xright; x += this.blocksize) {
                    this.renderBlock(context, x, ydown, this.downIndexes[level], this.downIndexBuffer[level]);
                    this.renderBlock(context, x, yup, this.upIndexes[level], this.upIndexBuffer[level]);
                }
            }
        }
    }

    render(context) {
        // set transformation uniforms
        this.setTransformationUniforms(context);

        let positionLoc = gl.getAttribLocation(context.shader, 'a_position');
        if(isValidAttributeLocation(positionLoc)) {
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
            gl.enableVertexAttribArray(positionLoc);
            gl.vertexAttribPointer(positionLoc, 3, gl.FLOAT, false, 0, 0);
        }

        let normalLoc = gl.getAttribLocation(context.shader, 'a_normal');
        if(isValidAttributeLocation(normalLoc)) {
            gl.bindBuffer(gl.ARRAY_BUFFER, this.normalBuffer);
            gl.enableVertexAttribArray(normalLoc);
            gl.vertexAttribPointer(normalLoc, 3, gl.FLOAT, false, 0, 0);
        }

        let texLoc = gl.getAttribLocation(context.shader, 'a_texCoord');
        if(isValidAttributeLocation(texLoc)) {
            gl.bindBuffer(gl.ARRAY_BUFFER, this.textureBuffer);
            gl.enableVertexAttribArray(texLoc);
            gl.vertexAttribPointer(texLoc, 2, gl.FLOAT, false, 0, 0);
        }

        //tell shader to use our texture
        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableHeightTexture'), 1);

        gl.uniform2fv(gl.getUniformLocation(context.shader, 'u_upperLeftHeightTexturePosition'), [0., 0.]);
        gl.uniform2fv(gl.getUniformLocation(context.shader, 'u_lowerRightHeightTexturePosition'), [this.size, this.size]);

        //const modelViewMatrix = mat4.multiply(mat4.create(), context.viewMatrix, context.sceneMatrix);
        this._lastBoundLevel = -1;

        const curPos = camera.getWorldPosition();
        const curBlockX = Math.max(0, Math.min(this.size-this.blocksize*2, parseInt((curPos[0] - this.blocksize/2) / this.blocksize) * this.blocksize));
        const curBlockY = Math.max(0, Math.min(this.size-this.blocksize*2, parseInt((curPos[1] - this.blocksize/2) / this.blocksize) * this.blocksize));

        this.renderBlockQuader(context, curBlockX, curBlockY, curBlockX + this.blocksize, curBlockY + this.blocksize, 0, 0);
        for(let i = 1; i < this.maxLOD; i++) {
            this.renderBlockQuader(context, curBlockX - this.blocksize*i, curBlockY - this.blocksize*i, curBlockX + this.blocksize*(i+1), curBlockY + this.blocksize*(i+1), i, i-1);
        }
        // TODO: limit to LOD and visibility range
        const numBlocks = parseInt(this.size / this.blocksize);
        for(let i = this.maxLOD; i < numBlocks; i++) {
            this.renderBlockQuader(context, curBlockX - this.blocksize*i, curBlockY - this.blocksize*i, curBlockX + this.blocksize*(i+1), curBlockY + this.blocksize*(i+1), this.maxLOD-1, this.maxLOD-1);
        }

        // Unbind the buffer
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        if(isValidAttributeLocation(normalLoc)) {
            gl.disableVertexAttribArray(normalLoc);
        }
        if(isValidAttributeLocation(positionLoc)) {
            gl.disableVertexAttribArray(positionLoc);
        }
        if(isValidAttributeLocation(texLoc)) {
            gl.disableVertexAttribArray(texLoc);
        }

        gl.uniform1i(gl.getUniformLocation(context.shader, 'u_enableHeightTexture'), 0);

        //render children
        super.render(context);
    }


    getZPos(x,y){

        let color = getPixel( imageData, Math.abs(Math.round(y)), Math.abs(Math.round(x)) );

        return color.r*0.78125; // 200 / 256
    }
}

/*
 Taken from: https://github.com/mrdoob/three.js/issues/758
 */
function getImageData( ) {

    let image = document.getElementById('heightmap');
    var canvas = document.createElement( 'canvas' );
    canvas.width = image.width;
    canvas.height = image.height;

    var context = canvas.getContext( '2d' );
    context.drawImage( image, 0, 0 );

    return context.getImageData( 0, 0, image.width, image.height );

}

function getPixel( imagedata, x,y ) {

    var position = ( y + imagedata.width * x) * 4, data = imagedata.data;
    position = position.toFixed(0) ;
    return { r: data[ position], g: data[ position + 1 ], b: data[ position + 2 ], a: data[ position + 3 ] };

}


