/**
 * Create a simple tree object with two LOD levels.
 *
 * @param resources
 * @returns {LodSGNode}
 */
function createTreeObj(resources) {
    let lod0 = new RenderSGNode(resources.tree3);
    let lod1 = new TransformationSGNode(glm.translate(0,0,5), [
        new BillboardSGNode([
            new TextureSGNode(resources.tree3_billbaord, [
                new TransformationSGNode(glm.scale(1, -1, 1), new RenderSGNode(makeRect(5,5))),
            ])
        ])
    ]);

    return new LodSGNode(200, [lod0, lod1]);
}

/**
 * Create a simple grass object with 3 LOD levels.
 *
 * @param resources
 * @returns {LodSGNode}
 */
function createGrassObj(resources) {
    let lod0 = new WoobleSGNode(new TextureSGNode(resources.tree3_texture, [
        new RenderSGNode(resources.plant)
    ]));
    lod0.woobleHeight = 1.;
    lod0.woobleAmplitude = [0.5, 1., -0.5];

    let lod1 = new TransformationSGNode(glm.translate(0,0,0.5), [
        new BillboardSGNode([
            new TextureSGNode(resources.grass_billboard, [
                new TransformationSGNode(glm.scale(1, -1, 1), new RenderSGNode(makeRect(1,1)))
            ])
        ])
    ]);

    return new LodSGNode(200, [lod0, lod1, lod1, null]);
}

/**
 * Create a simple grass object with 3 LOD levels.
 *
 * @param resources
 * @returns {LodSGNode}
 */
function createPlantObj(resources) {
    let lod0 = new WoobleSGNode(new TextureSGNode(resources.tree3_texture, [
        new RenderSGNode(resources.plant)
    ]));
    lod0.woobleHeight = 2.;
    lod0.woobleAmplitude = [0.5, 1., -0.5];

    let lod1 = new TransformationSGNode(glm.translate(0,0,0.5), [
        new BillboardSGNode([
            new TextureSGNode(resources.plant_billboard, [
                new TransformationSGNode(glm.scale(1, -1, 1), new RenderSGNode(makeRect(1,1)))
            ])
        ])
    ]);

    return new LodSGNode(200, [lod0, lod1, lod1, null]);
}

/**
 * Create a simple grass object with 3 LOD levels.
 *
 * @param resources
 * @returns {LodSGNode}
 */
function createMushroomObj(resources) {
    let lod0 = new WoobleSGNode(new TextureSGNode(resources.tree3_texture, [
        new RenderSGNode(resources.mushroom1)
    ]));
    lod0.woobleHeight = 2.;

    let lod1 = new TransformationSGNode(glm.translate(0,0,0.25), [
        new BillboardSGNode([
            new TextureSGNode(resources.mushroom1_billboard, [
                new TransformationSGNode(glm.scale(1, -1, 1), new RenderSGNode(makeRect(0.5,0.5))),
            ])
        ])
    ]);

    return new LodSGNode(200, [lod0, lod1 , lod1, null]);
}

/**
 * Create a House
 *
 * @param resources
 * @returns {TextureSGNode}
 */
function createHouse(resources) {
    let house = new RenderSGNode(resources.house2);
    house.append(new TransformationSGNode(glm.transform({translate:[1.6,-0.2,3.5], scale: [0.4, 0.4, 0.4], rotateY: 90, rotateX: -90}),
            new ClockObjectSGSGNode(resources)
        )
    );

    return new TextureSGNode(resources.house2_texture,
        new LoadMtlSGNode(resources.house2_mtl,
            new TransformationSGNode(glm.scale(14, 14, 25),
                house
            )
        )
    );
}

/**
 * Create a Lighthouse including rotating light
 *
 * @param resources
 * @returns {ScriptSGNode}
 */
function createLighthouse(resources) {
    // create lighthouse model
    let lighthouse = new LoadMtlSGNode(resources.lighthouse_mtl,
        new TransformationSGNode(glm.scale(14, 14, 14),
            //new ClockObjectSGSGNode()
            new TextureSGNode(resources.lighthouse_texture,
                new RenderSGNode(resources.lighthouse) // TODO: new Model(resources.lighthouse)
        ))
    );

    //initialize light
    let light = new SpotLightSGNode(); //use now framework implementation of light node
    light.ambient = [0.1, 0.05, 0.05, 1];
    light.diffuse = [0.8, 0.7, 0.7, 1];
    light.specular = [0.5, 0.5, 0.5, 1];
    light.position = [0, 0, 0];
    light.direction = [1, 0, -1];
    light.angle = 30;

    // wee need a node, which we can rotate
    let lightRotationNode = new TransformationSGNode(mat4.create(), [
        light,
        createLightSphere(5)
    ]);

    // add light relative to house position
    lighthouse.append(new TransformationSGNode(glm.translate(0, 0, 134), [
        lightRotationNode
    ]));

    // rotate node based on current time in milliseconds
    return new ScriptSGNode(
        function(context) {
            lightRotationNode.matrix = glm.rotateZ(context.timeInMilliseconds*0.05);
        },
        lighthouse
    );
}

/**
 * create a Light Sphere which we can use for debugging
 *
 * @param diameter
 * @returns {MaterialSGNode}
 */
function createLightSphere(diameter) {
    // create light sphere
    let material = new MaterialSGNode(
        new RenderSGNode(
            makeSphere(diameter, 10, 10)
        )
    );

    // our node is fully white
    material.ambient = [0, 0, 0, 1];
    material.diffuse = [0, 0, 0, 1];
    material.specular = [0, 0, 0, 1];
    material.emission = [1, 1, 1, 1];
    material.shininess = 0.0;

    return material;
}

/**
 * create a glowing hy which always looks to the camera
 *
 * @param resources
 * @returns {*}
 */
function createHyBillboard(resources) {
    let material = new MaterialSGNode(new TextureSGNode(resources.hy, [
        new BillboardSGNode(new TransformationSGNode(glm.transform({scale:[1, -1, 1]}), [
            new RenderSGNode(makeRect(10, 10))
        ]))
    ]));

    material.ambient = [0.3, 0.1, 0.1, 1];
    material.diffuse = [0.5, 0.2, 0.2, 1];
    material.specular = [0.3, 0, 0, 1];
    material.emission = [0.6, 0.2, 0.2, 1];
    material.shininess = 0.0;

    return material;
}

/**
 * Create Sun including floating billboard
 *
 * @param resources
 * @returns {DirectionalLightSGNode}
 */
function createSun(resources) {
    // create light node
    let sun = new DirectionalLightSGNode();

    sun.ambient = [0.2, 0.2, 0.2, 1];
    sun.diffuse = [0.8, 0.8, 0.8, 1];
    sun.specular = [0.5, 0.5, 0.5, 1];

    // create billboard with material and texture
    let material = new MaterialSGNode(new BillboardSGNode([
        new TransformationSGNode(glm.transform({rotateZ:90}), [
            new TextureSGNode(resources.sun, [
                new RenderSGNode(makeRect(256, 256)),
            ])
        ])
    ]));
    material.ambient = [.0, .0, .0, 1];
    material.diffuse = [.0, .0, .0, 1];
    material.specular = [.0, .0, .0, 1];
    material.emission = [0.78, 1., 0.23, 1];

    // create transformation
    let transformation = new TransformationSGNode(mat4.create(), [
        material
    ]);

    let cameraFollower = new CameraFollowSGNode([
        new ScriptSGNode(function(context) {
            // calculate sun position based on our current time
            sun.direction = vec3.rotateY(vec3.create(), [0, 0, 1], [0, 0, 0], glm.deg2rad(context.globalTimeInMilliseconds/(24*60*60*1000)*360));
            sun.direction = vec3.rotateX(vec3.create(), sun.direction, [0, 0, 0], glm.deg2rad(-45));

            // when the light goes under the horizon, we don't wan't it to light our scene from below.
            if(sun.direction[2] > 0.1) {
                sun.attenuation = 0.1 / sun.direction[2];
            } else {
                sun.attenuation = 1.;
            }

            let sunPosition = vec3.normalize(vec3.create(), sun.direction);
            sunPosition = vec3.negate(sunPosition, sunPosition);
            sunPosition = vec3.scale(sunPosition, sunPosition, 2000);

            transformation.matrix = mat4.fromTranslation(mat4.create(), sunPosition);
        }, transformation)
    ]);
    sun.append(cameraFollower);

    return sun;
}

function createAnimatedLightNode(resources) {
    let light = new LightSGNode(); //use now framework implementation of light node
    light.ambient = [0.2, 0.2, 0.2, 1];
    light.diffuse = [0.8, 0.3, 0.3, 1];
    light.specular = [0.3, 0.1, 0.1, 1];
    light.position = [0, 0, 0];
    light.uniform = "u_pointlight";

    let rotateLight = new TransformationSGNode(mat4.create());
    let translateLight = new TransformationSGNode(glm.translate(15, 0, 0)); //translating the light is the same as setting the light position

    rotateLight.append(translateLight);
    translateLight.append(light);
    translateLight.append(createLightSphere(3)); //add sphere for debugging: since we use 0,0,0 as our light position the sphere is at the same position as the light source

    return new ScriptSGNode(function(context) {
        rotateLight.matrix = glm.rotateZ(context.timeInMilliseconds*0.05);
    }, rotateLight);
}

/**
 * Create our terrain including water
 *
 * @param resources
 * @returns {OurAdvancedTextureSGNode}
 */
function createTerrain(resources) {
    // create terrain
    terrain = new TerrainSGNode(1024, 64);

    // create texture node
    let terrainTexture = new AdvancedTextureSGNode(resources.worldtexture, [
        new SetUniformSGNode("u_enableObjectTexture", true),
        terrain, // TODO: RenderSGNode does not work with this node
        new SetUniformSGNode("u_enableObjectTexture", false)
    ]);

    let water = new RenderSGNode(makeRect(1024/2, 1024/2));

    // create material node
    let waterTexture = new TextureSGNode(resources.watertexture, [
        new TransformationSGNode(glm.translate(-1024/2, -1024/2, 0), water),
        new TransformationSGNode(glm.translate(-1024/2, +1024/2, 0), water),
        new TransformationSGNode(glm.translate(-1024/2, +1024*3/2, 0), water),
        new TransformationSGNode(glm.translate(+1024/2, +1024*3/2, 0), water),
        new TransformationSGNode(glm.translate(+1024*3/2, +1024*3/2, 0), water),
        new TransformationSGNode(glm.translate(+1024*3/2, +1024/2, 0), water),
        new TransformationSGNode(glm.translate(+1024*3/2, -1024/2, 0), water),
        new TransformationSGNode(glm.translate(+1024/2, -1024/2, 0), water)
    ]);

    // create material node
    let terrainMaterial = new MaterialSGNode([terrainTexture, waterTexture]);

    //TODO: gold
    terrainMaterial.ambient = [0.6, 0.6, 0.6, 1];
    terrainMaterial.diffuse = [0.75164, 0.60648, 0.22648, 1];
    terrainMaterial.specular = [0.628281, 0.555802, 0.366065, 1];
    terrainMaterial.shininess = 0.9;

    // add heightmap
    let heightmap = new OurAdvancedTextureSGNode(resources.heightmap, [terrainMaterial]);
    heightmap.uniform = 'u_HeightTexture';
    heightmap.textureunit = 4;

    return heightmap;
}

/**
 * Create Cube
 *
 * @param s
 * @returns {{position: Float32Array, normal: Float32Array, texture: Float32Array, index: Float32Array}}
 */
function createCube(s) {
    let vertex = new Float32Array([
        -s, -s, -s, s, -s, -s, /**/ s, s, -s, -s, s, -s,
        -s, -s, s, s, -s, s, /**/ s, s, s, -s, s, s,
        -s, -s, -s, -s, s, -s, /**/ -s, s, s, -s, -s, s,
        s, -s, -s, s, s, -s, /**/ s, s, s, s, -s, s,
        -s, -s, -s, -s, -s, s, /**/ s, -s, s, s, -s, -s,
        -s, s, -s, -s, s, s, /**/ s, s, s, s, s, -s,
    ]);

    let index = new Float32Array([
        0, 1, 2, 0, 2, 3,
        4, 5, 6, 4, 6, 7,
        8, 9, 10, 8, 10, 11,
        12, 13, 14, 12, 14, 15,
        16, 17, 18, 16, 18, 19,
        20, 21, 22, 20, 22, 23
    ]);

    let texture = new Float32Array([
        0, 0, 1 / 4, 0, 1 / 4, 1 / 4, 0, 0, //FRONT
        //Front has the upper quarter of the texture
        1 / 4, 0, 2 / 4, 0, 2 / 4, 1 / 4, 1 / 4, 0, //BACK
        2 / 4, 0, 3 / 4, 0, 3 / 4, 1 / 4, 2 / 4, 0, //LEFT
        3 / 4, 0, 1, 0, 1, 1 / 4, 3 / 4, 0, //RIGHT
        0, 1 / 4, 1 / 4, 1 / 4, 1 / 4, 2 / 4, 0, 1 / 4, //TOP
        1 / 4, 1 / 4, 2 / 4, 1 / 4, 2 / 4, 2 / 4, 1 / 4, 1 / 4  //BOTTOM
    ]);

    let normal = new Float32Array([
        0.0, 0.0, 1.0, 0.0, 0.0, 1.0,
        0.0, 0.0, 1.0, 0.0, 0.0, 1.0,
        0.0, 0.0, 1.0, 0.0, 0.0, 1.0,
        0.0, 0.0, 1.0, 0.0, 0.0, 1.0,
        1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
        1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
        1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
        1.0, 0.0, 0.0, 1.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0, 1.0, 0.0,
        0.0, 1.0, 0.0, 0.0, 1.0, 0.0,
        0.0, 1.0, 0.0, 0.0, 1.0, 0.0,
        0.0, 1.0, 0.0, 0.0, 1.0, 0.0
    ]);

    return {
        position: vertex,
        normal: normal,
        texture: texture,
        index: index
    }
}

/**
 * Create Arrow for clock
 *
 * @param l Length of arrow
 * @param w width of arrow
 * @param h height of arrow
 *
 * @returns {{position: Float32Array, normal: Float32Array, texture: Float32Array, index: Float32Array}}
 */
function createArrow(l, w, h) {
    let w2 = w / 2.;
    let w4 = w / 4.;
    let ld = -w2;
    let lu = l - w2;

    let s = 1;
    let vertex = new Float32Array([
        /* lower part of model */
        -w4, ld, 0,     w4, ld, 0,
        w4, lu-w, 0,    -w4, lu-w, 0,
        -w2, lu-w, 0,   0, lu, 0,
        w2, lu-w, 0,
        /* upper part of model */
        w4, lu-w, h,    -w4, lu-w, h,
        -w4, ld, h,     w4, ld, h,
        -w2, lu-w, 0,   0, lu, 0,
        w2, lu-w, 0,
        /* lower side part of model */
        -w4, ld, 0,     w4, ld, 0,
        -w4, ld, h,     w4, ld, h,
        -w4, lu-w, 0,   -w2, lu-w, 0,   -w4, lu-w, h,
        w4, lu-w, 0,    w2, lu-w, 0,    w4, lu-w, h,
        /* right side part of model */
        -w4, ld, 0,     -w4, lu-w, 0,
        -w4, ld, h,     -w4, lu-w, h,
        /* left side part of model */
        w4, ld, 0,      w4, lu-w, 0,
        w4, ld, h,      w4, lu-w, h,
    ]);

    let index = new Float32Array([
        /* lower part of model */
        0, 1, 2,        0, 2, 3,
        3, 4, 5,        2, 3, 5,
        2, 5, 6,
        /* upper part of model */
        11, 12, 8,      12, 7, 8,
        12, 13, 7,      7, 9, 8,
        7, 10, 9,
        /* lower side part of model */
        14, 15, 16,     15, 17, 16,
        18, 19, 20,     21, 22, 23,
        /* right side part of model */
        24, 25, 26,     25, 26, 27,
        /* left side part of model */
        28, 29, 30,     29, 30, 31
    ]);

    let texture = new Float32Array([
        /* lower part of model */
        1/16, 0,    3/16, 0,
        3/16, 1/2,  1/16, 1/2,
        0, 1/2,     1/8, 1,
        1/4, 1/2,
        /* upper part of model */
        1/4+1/16, 0,    1/4+3/16, 0,
        1/4+3/16, 1/2,  1/4+1/16, 1/2,
        1/4+0, 1/2,     1/4+1/8, 1,
        1/4+1/4, 1/2,
        /* lower side part of model */
        1/2, 0,     3/4, 0,
        1/2, 1/2,   3/4, 1/2,
        3/4, 1/2,   1/2, 1/2, 3/4, 1,
        3/4, 1/2,   1/2, 1/2, 3/4, 1, // symetric
        /* right side part of model */
        3/4, 0,     3/4, 1,
        1, 0,       1, 1,
        /* left side part of model */
        3/4, 0,     3/4, 1,
        1, 0,       1, 1
    ]);

    let normal = new Float32Array([
        /* lower part of model */
        0.0, 0.0, -1.0,     0.0, 0.0, -1.0,
        0.0, 0.0, -1.0,     0.0, 0.0, -1.0,
        0.0, 0.0, -1.0,     0.0, 0.0, -1.0,
        0.0, 0.0, -1.0,
        /* TODO: upper part of model */
        0.0, 0.0, 1.0,      0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,      0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,      0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,
        /* lower side part of model */
        -1.0, 0.0, 0.0,     -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,     -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,     -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,     -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,     -1.0, 0.0, 0.0,
        /* right side part of model */
        0.0, -1.0, 0.0,     0.0, -1.0, 0.0,
        0.0, -1.0, 0.0,     0.0, -1.0, 0.0,
        /* left side part of model */
        0.0, 1.0, 0.0,      0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,      0.0, 1.0, 0.0,
    ]);

    return {
        position: vertex,
        normal: normal,
        texture: texture,
        index: index
    }
}