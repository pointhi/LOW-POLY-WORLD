/**
 * Created by Samuel Gratzl on 29.02.2016.
 */

#define M_PI 3.1415926535897932384626433832795

// attributes attached to every vertex
attribute vec3 a_position;
attribute vec3 a_normal;
attribute vec2 a_texCoord;

// some basic matrices
uniform mat4 u_modelView;
uniform mat3 u_normalMatrix;
uniform mat4 u_projection;

// some flags to enable specific parts of the shader
uniform bool u_enableHeightTexture;
uniform bool u_enableWooble;

// uniforms required for height texture
uniform vec2 u_upperLeftHeightTexturePosition;
uniform vec2 u_lowerRightHeightTexturePosition;
uniform vec2 u_HeightMatrixDisplacement;
uniform sampler2D u_HeightTexture;

// uniforms required to calculate wobble
uniform float u_woobleTime; // ms
uniform float u_wooblePeriode; // ms
uniform float u_woobleHeight;
uniform vec3 u_woobleAmplitude;

//output of this shader
varying vec3 v_normalVec;
varying vec3 v_eyeVec;
varying vec2 v_texCoord;

// pointlight
uniform vec3 u_pointlightPos;
varying vec3 v_pointlightVec;

// spotlight
uniform vec3 u_spotlightPos;
varying vec3 v_spotlightVec;

float height(vec2 pos) {
    vec2 heightTexPos = (pos.xy - u_upperLeftHeightTexturePosition.xy) / u_lowerRightHeightTexturePosition.xy;
    return texture2D(u_HeightTexture,heightTexPos).r * 200.;
}

void main() {
    // that's the base vector position, which we can further transform
    vec3 position = a_position;

    // we probably override this vector
    v_normalVec = u_normalMatrix * a_normal;
    //v_normalVec = a_normal;
    v_texCoord = a_texCoord;

    // use heightmap for z vector and normal mapping
    if(u_enableHeightTexture) {
        position.xy += u_HeightMatrixDisplacement;

        position.z += height(position.xy);

        // read neightbor heights using an arbitrary small offset
        // https://stackoverflow.com/questions/13983189/opengl-how-to-calculate-normals-in-a-terrain-height-grid
        const vec3 off = vec3(1.0, 1.0, 0.0);
        float hL = height(position.xy - off.xz);
        float hR = height(position.xy + off.xz);
        float hD = height(position.xy - off.zy);
        float hU = height(position.xy + off.zy);

        // deduce terrain normal
        v_normalVec = u_normalMatrix * normalize(vec3(hL - hR, hD - hU, 2.0)); // TODO: normal matrix?

        // TODO: for testing purpose
        v_texCoord = (position.xy - u_upperLeftHeightTexturePosition.xy) / u_lowerRightHeightTexturePosition.xy;
    }

    // move object over time using a sinus curve
    if(u_enableWooble) {
        // do not wooble as much on the lower part
        float woobleMultiplier = pow(position.z / u_woobleHeight, 2.);
        // calculate how much we want to wooble the specified vertex
        float woobleOffset = sin(u_woobleTime/u_wooblePeriode * 2. * M_PI) * woobleMultiplier;

        vec3 woobleOffsetVec = vec3(woobleOffset, woobleOffset, woobleOffset);

        if(woobleOffsetVec.z < 0.) {
            woobleOffsetVec.z = -woobleOffsetVec.z;
        }
        // calculate change of position
        position += woobleOffsetVec * u_woobleAmplitude;
    }

	vec4 eyePosition = u_modelView * vec4(position,1);

    v_eyeVec = -eyePosition.xyz;

    // calculate light vectors
    v_pointlightVec = u_pointlightPos - eyePosition.xyz;
    v_spotlightVec = u_spotlightPos - eyePosition.xyz;

	gl_Position = u_projection * eyePosition;
}
