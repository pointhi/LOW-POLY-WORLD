/**
 * a phong shader implementation with texture support
 */

precision mediump float;

/**
 * definition of a material structure containing common properties
 */
struct Material {
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emission;
	float shininess;
};

/**
 * definition of the light properties related to material properties
 */
struct Light {
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
};

//illumination related variables
uniform Material u_material;

varying vec3 v_normalVec;
varying vec3 v_eyeVec;

// directional light (sun)
uniform Light u_dirlight;
uniform vec3 u_dirlightDir;

// point light
uniform Light u_pointlight;
varying vec3 v_pointlightVec;

// spot light
uniform Light u_spotlight;
uniform vec3 u_spotlightDir;
uniform float u_spotlightAngle;
varying vec3 v_spotlightVec;

//texture related variables
uniform bool u_enableObjectTexture;
varying vec2 v_texCoord;
uniform sampler2D u_tex;

vec4 calculateDirectionalLight(Light light, Material material, vec3 lightVec, vec3 normalVec, vec3 eyeVec) {
    lightVec = normalize(lightVec);
	normalVec = normalize(normalVec);
	eyeVec = normalize(eyeVec);

    //compute diffuse term
    float diffuse = max(dot(-normalVec,lightVec),0.0);

    //compute specular term
    vec3 reflectVec = reflect(lightVec, normalVec);
    float spec = pow( max( dot(reflectVec, eyeVec), 0.0) , material.shininess);

    vec4 c_amb  = clamp(light.ambient * material.ambient, 0.0, 1.0);
    vec4 c_diff = clamp(diffuse * light.diffuse * material.diffuse, 0.0, 1.0);
    vec4 c_spec = clamp(spec * light.specular * material.specular, 0.0, 1.0);
    vec4 c_em   = material.emission;

    return c_amb + c_diff + c_spec + c_em;
}

vec4 calculateSpotLight(Light light, Material material, vec3 lightVec, vec3 lightDir, float angle, float attenuation, vec3 normalVec, vec3 eyeVec) {
    float distance = length(lightVec);

    float brightness = 1.0 / (1.0 + attenuation * distance * distance);

    // those calculations are only required when the angle is less than 180 degree
    if(angle < 180.) {
        float lightToSurfaceAngle = degrees(acos(dot(-normalize(lightVec), normalize(lightDir))));
        if(lightToSurfaceAngle > angle){
            brightness = 0.0;
        }
    }

	lightVec = normalize(lightVec);
	normalVec = normalize(normalVec);
	eyeVec = normalize(eyeVec);

	//compute diffuse term
	float diffuse = max(dot(normalVec,lightVec),0.0);

	//compute specular term
	vec3 reflectVec = reflect(-lightVec,normalVec);
	float spec = pow( max( dot(reflectVec, eyeVec), 0.0) , material.shininess);

	vec4 c_amb  = clamp(light.ambient * material.ambient, 0.0, 1.0);
	vec4 c_diff = clamp(diffuse * light.diffuse * material.diffuse, 0.0, 1.0);
	vec4 c_spec = clamp(spec * light.specular * material.specular, 0.0, 1.0);
	vec4 c_em   = material.emission;

    return (c_amb + c_diff + c_spec + c_em) * brightness;
}

vec4 calculatePointLight(Light light, Material material, vec3 lightVec, float attenuation, vec3 normalVec, vec3 eyeVec) {
    // we can take on a point light is a special case of a spotlight with an angle of 180 degree
    return calculateSpotLight(light, material, lightVec, vec3(0., 0., -1.), 180., attenuation, normalVec, eyeVec);
}

void main() {
    vec4 textureColor = vec4(0.6,0.6,0.6,1);

    Material material = u_material;

    if(u_enableObjectTexture)
      {
        textureColor = texture2D(u_tex,v_texCoord);
        material.diffuse = textureColor;
        material.ambient = textureColor;
    	//Note: an alternative to replacing the material color is to multiply it with the texture color
      }

    // normalize some vectors
    vec3 normalVec = normalize(v_normalVec);
    vec3 eyeVec = normalize(v_eyeVec);

    // calculate global light
    vec3 dirlightVec = normalize(u_dirlightDir);

    vec4 fragColor = vec4(0., 0., 0., 0.);
    fragColor += calculateDirectionalLight(u_dirlight, material, dirlightVec, normalVec, eyeVec);
    fragColor += calculatePointLight(u_pointlight, material, v_pointlightVec, 0.0001, normalVec, eyeVec);
    fragColor += calculateSpotLight(u_spotlight, material, v_spotlightVec, u_spotlightDir, u_spotlightAngle, 0.00002, normalVec, eyeVec);

    fragColor.w = textureColor.w; // TODO: how to handle alpha gracefully?

    if(fragColor.w == 0.0) {
        discard; // don't write into the z-buffer when nothing visible appears
    } else {
        gl_FragColor = fragColor;
    }
}
